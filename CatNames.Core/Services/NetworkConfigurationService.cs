﻿using Base.Network.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatNames.Core.Services
{
    public class NetworkConfigurationService : INetworkConfigurationService
    {
        public NetworkConfigurationService()
        {
            // Set up default values, should ideally come from a configuration / resx file
            Timeout = 5000;
            NetworkRetryCount = 5;
            ApiBase = "http://agl-developer-test.azurewebsites.net/";
        }

        private const int RetryCount = 5;

        public int NetworkRetryCount { get; set; }

        public string ApiBase { get; set; }

        public int Timeout { get; set; }
    }
}

﻿using Base.Network.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CatNames.Core.Services
{
    public class ObjectMapperService : IObjectMapperService
    {
        public T DeserializeObject<T>(string serializedObject)
        {
            return JsonConvert.DeserializeObject<T>(serializedObject);
        }

        public string SerializeObject(object toConvert)
        {
            return JsonConvert.ToString(toConvert);
        }
    }
}

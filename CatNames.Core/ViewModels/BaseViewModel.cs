﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatNames.Core.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
        public abstract Task OnStartAsync();

        public override void Start()
        {
            base.Start();

            Task.Run(async () =>
            {
                await OnStartAsync();
            });
        }
    }
}

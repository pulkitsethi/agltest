﻿using CatNames.Core.Models;
using CatNames.Core.Repository.Interfaces;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CatNames.Core.ViewModels
{
    public class CatNamesViewModel : BaseViewModel
    {
        #region readonly

        private IOwnerRemoteRepository _ownerRemoteRepository;

        #endregion

        #region Constructor

        public CatNamesViewModel(IOwnerRemoteRepository ownerRemoteRepository)
        {
            _ownerRemoteRepository = ownerRemoteRepository;

            _petNames = new ObservableCollection<string>();
        }

        #endregion

        #region Private members

        private bool _isLoading = false;
        private ICommand _refreshCommand = null;
        private ObservableCollection<string> _petNames = new ObservableCollection<string>();

        #endregion

        #region Commands

        public ICommand RefreshCommand => _refreshCommand ?? (_refreshCommand = new MvxAsyncCommand(ExecuteReloadNames));

        #endregion

        #region Properties
        public ObservableCollection<string> PetNames
        {
            get { return _petNames; }

            set
            {
                _petNames = value;
                RaisePropertyChanged(() => PetNames);
            }
        }

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                RaisePropertyChanged(() => IsLoading);
            }
        }
        #endregion

        public override async Task OnStartAsync()
        {
            await LoadCatNamesByOwnerGender();
        }

        private async Task ExecuteReloadNames()
        {

            await LoadCatNamesByOwnerGender()
                .ConfigureAwait(false);
        }

        private async Task LoadCatNamesByOwnerGender()
        {
            IsLoading = true;

            var data = await _ownerRemoteRepository.GetOwnerList();

            // Create result collection
            var result = new ObservableCollection<string>();

            if(null != data)
            {
                var s = data.Where(x => null != x.pets)
                    .SelectMany(x => x.pets.Where(p => p.type == PetType.Cat), (o, p) => new { OwnerGender = o.gender, PetName = p.name });

                // Add first header
                result.Add(GenderType.Male);
                // male 
                foreach (string name in s.Where(x => x.OwnerGender == GenderType.Male).
                    OrderBy(x => x.PetName).Select(x => x.PetName))
                {
                    result.Add(name);
                }

                // Add second header
                result.Add(GenderType.Female);
                // female
                foreach (string name in s.Where(x => x.OwnerGender == GenderType.Female).
                    OrderBy(x => x.PetName).Select(x => x.PetName))
                {
                    result.Add(name);
                }
            }

            // set result
            PetNames = result;

            IsLoading = false;

        }
    }
}

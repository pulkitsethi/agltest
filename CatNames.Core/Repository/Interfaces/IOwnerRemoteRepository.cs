﻿using CatNames.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatNames.Core.Repository.Interfaces
{
    public interface IOwnerRemoteRepository
    {
        Task<IList<Owner>> GetOwnerList();
    }
}

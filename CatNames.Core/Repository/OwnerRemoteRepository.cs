﻿using CatNames.Core.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CatNames.Core.Models;
using Base.Network.Interfaces;

namespace CatNames.Core.Repository
{
    public class OwnerRemoteRepository : IOwnerRemoteRepository
    {
        #region readonly 

        private readonly INetworkService _networkService;

        #endregion

        #region Constructor

        public OwnerRemoteRepository(INetworkService networkService)
        {
            _networkService = networkService;
        }

        #endregion

        public async Task<IList<Owner>> GetOwnerList()
        {
            var result = await _networkService.GetAsync<IList<Owner>>("people.json")
                .ConfigureAwait(false);

            if (null != result)
            {
                return result.Data;
            }

            return null;
        }
    }
}

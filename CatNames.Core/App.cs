﻿using Base.Network.Interfaces;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using System.Reflection;

namespace CatNames.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            base.Initialize();

            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsSingleton();

            CreatableTypes(typeof(Base.Network.Services.NetworkService).GetTypeInfo().Assembly)
                .EndingWith("Service").
                AsInterfaces().
                RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("Repository")
                .AsInterfaces()
                .RegisterAsSingleton();

            var s = Mvx.Resolve<INetworkService>();

            // Register app start
            RegisterAppStart(new AppStart());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatNames.Core.Models
{
    public class Owner
    {
        public string name { get; set; }

        public string gender { get; set; }

        public int age { get; set; }

        public IList<Pet> pets { get; set; }

    }

    public class GenderType
    {
        public const string Male = "Male";

        public const string Female = "Female";
    }
}

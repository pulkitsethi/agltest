﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatNames.Core.Models
{
    public class Pet
    {
        public string name { get; set; }

        public string type { get; set; }
    }

    public class PetType
    {
        public const string Cat = "Cat";
        public const string Dog = "Dog";
        public const string Fish = "Fish";
    }
}

﻿using Base.Network.Interfaces;
using CatNames.Core.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatNames.Test.Services
{
    public class NetworkConfigurationServiceTests : BaseTest
    {
        private const string AppBaseUrl = "http://agl-developer-test.azurewebsites.net/";
        private INetworkConfigurationService _networkConfigurationService;

        [SetUp]
        protected void SetUp()
        {
            base.Setup();
        }

        [Test]
        public void TestDefaultConfiguration()
        {
            _networkConfigurationService = new NetworkConfigurationService();

            Assert.AreEqual(5000, _networkConfigurationService.Timeout);
            Assert.AreEqual(5, _networkConfigurationService.NetworkRetryCount);
            Assert.AreEqual(AppBaseUrl, _networkConfigurationService.ApiBase);
        }
    }
}

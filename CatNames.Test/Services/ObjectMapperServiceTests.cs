﻿using Base.Network.Interfaces;
using CatNames.Core.Models;
using CatNames.Core.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatNames.Test.Services
{
    public class ObjectMapperServiceTests : BaseTest
    {
        private const string ownerJson = "{\"name\":\"Bob\",\"gender\":\"Male\",\"age\":23,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"},{\"name\":\"Fido\",\"type\":\"Dog\"}]}";

        private readonly Owner Owner = new Owner
        {
            name = "Bob",
            gender = "Male",
            age = 23,
            pets = new List<Pet>
            {
                new Pet
                {
                    name = "Garfield",
                    type = PetType.Cat
                },
                new Pet
                {
                    name = "Fido",
                    type = PetType.Dog
                }
            }
        };

        private IObjectMapperService _objectMapperService;

        [SetUp]
        protected void SetUp()
        {
            base.Setup();
        }

        [Test]
        public void TestConversion()
        {
            CreateObjectMapperService();

            var data = _objectMapperService.DeserializeObject<Owner>(ownerJson);

            Assert.AreEqual(data.name, Owner.name);
            Assert.AreEqual(data.gender, Owner.gender);
            Assert.AreEqual(data.age, Owner.age);
            Assert.AreEqual(data.pets.Count, Owner.pets.Count);

            Assert.AreEqual(data.pets[0].type, Owner.pets[0].type);
        }

        private void CreateObjectMapperService()
        {
            _objectMapperService = new ObjectMapperService();
        }
    }
}

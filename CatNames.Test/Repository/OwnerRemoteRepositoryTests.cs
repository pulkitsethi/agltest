﻿using Base.Network.Interfaces;
using CatNames.Core.Models;
using CatNames.Core.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;
using CatNames.Core.Repository.Interfaces;
using NSubstitute;
using NUnit.Framework;
using Base.Network.Model;

namespace CatNames.Test.Services
{
    [TestFixture]
    public class OwnerRemoteRepositoryTests : BaseTest
    {
        private IList<Owner> _testData;
        private INetworkService _networkService;

        private IOwnerRemoteRepository _ownerRemoteRepository;

        [SetUp]
        protected void SetUp()
        {
            base.Setup();

            CreateOwnerData();

            _networkService = Substitute.For<INetworkService>();
        }

        [Test]
        public async Task GetOwnerListTest()
        {
            var result = new ApiResult<IList<Owner>>()
            {
                Result = true,
                Data = _testData,
                Message = string.Empty
            };

            _networkService.GetAsync<IList<Owner>>("people.json").Returns(Task.FromResult<ApiResult<IList<Owner>>>(result));

            CreateOwnerRemoteRepository();

            var data = await _ownerRemoteRepository.GetOwnerList();

            Assert.NotNull(data);
            Assert.AreEqual(3, data.Count);
            Assert.AreEqual("Nikki", data[2].name);
            Assert.AreEqual("Arru", data[1].pets[0].name);
            Assert.AreEqual(PetType.Cat, data[1].pets[1].type);
        }

        private void CreateOwnerRemoteRepository()
        {
            _ownerRemoteRepository = new OwnerRemoteRepository(_networkService);
        }

        private void CreateOwnerData()
        {
            _testData = new List<Owner>
            {
                new Owner()
                {
                    name = "Peter",
                    age = 26,
                    gender = GenderType.Male,
                    pets = new List<Pet>
                    {
                        new Pet
                        {
                            name = "Charlie",
                            type = PetType.Cat
                        },
                        new Pet
                        {
                            name = "Mika",
                            type = PetType.Dog
                        }
                    }
                },
                new Owner ()
                {
                    name = "Rolna",
                    age = 34,
                    gender = GenderType.Female,
                    pets = new List<Pet>
                    {
                        new Pet
                        {
                            name = "Arru",
                            type = PetType.Dog
                        },
                        new Pet
                        {
                            name = "Arru",
                            type = PetType.Cat
                        }
                    }
                },
                new Owner
                {
                    name = "Nikki",
                    age = 33,
                    gender = GenderType.Female,
                    pets = null

                }
            };
        }
    }
}

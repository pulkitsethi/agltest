﻿using System;
using MvvmCross.Test.Core;

namespace CatNames.Test
{
    /// <summary>
    /// Base class for all tests
    /// </summary>
    public class BaseTest : MvxIoCSupportingTest
    {
        protected override void AdditionalSetup()
        {
            // Put all global dependencies (mocked) here
            base.AdditionalSetup();
        }
    }
}

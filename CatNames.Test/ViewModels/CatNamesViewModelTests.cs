﻿using CatNames.Core.Models;
using CatNames.Core.Repository;
using CatNames.Core.Repository.Interfaces;
using CatNames.Core.ViewModels;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatNames.Test.ViewModels
{
    public class CatNamesViewModelTests : BaseTest
    {
        private IOwnerRemoteRepository _ownerRemoteRepository;
        private IList<Owner> _ownerData;

        private CatNamesViewModel _catNamesViewModel;

        [SetUp]
        protected void SetUp()
        {
            base.Setup();

            _ownerRemoteRepository = Substitute.For<IOwnerRemoteRepository>();
        }



        [Test]
        public async Task LoadTest()
        {
            CreateOwnerData();

            _ownerRemoteRepository.GetOwnerList().Returns(Task.FromResult(_ownerData));
            CreateViewModel();

            await _catNamesViewModel.OnStartAsync();

            Assert.AreEqual(4, _catNamesViewModel.PetNames.Count);
            Assert.AreEqual("Charlie", _catNamesViewModel.PetNames[1]);
            Assert.AreEqual("Arru", _catNamesViewModel.PetNames[3]);
        }

        private void CreateViewModel()
        {
            _catNamesViewModel = new CatNamesViewModel(_ownerRemoteRepository);
        }

        private void CreateOwnerData()
        {
            _ownerData = new List<Owner>
            {
                new Owner()
                {
                    name = "Peter",
                    age = 26,
                    gender = GenderType.Male,
                    pets = new List<Pet>
                    {
                        new Pet
                        {
                            name = "Charlie",
                            type = PetType.Cat
                        },
                        new Pet
                        {
                            name = "Mika",
                            type = PetType.Dog
                        }
                    }
                },
                new Owner ()
                {
                    name = "Rolna",
                    age = 34,
                    gender = GenderType.Female,
                    pets = new List<Pet>
                    {
                        new Pet
                        {
                            name = "Arru",
                            type = PetType.Dog
                        },
                        new Pet
                        {
                            name = "Arru",
                            type = PetType.Cat
                        }
                    }
                },
                new Owner
                {
                    name = "Nikki",
                    age = 33,
                    gender = GenderType.Female,
                    pets = null

                }
            };
        }
    }
}

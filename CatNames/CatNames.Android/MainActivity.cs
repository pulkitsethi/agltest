﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using MvvmCross.Droid.Views;

namespace CatNames.Droid
{
	[Activity (Label = "Cat Names", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : MvxSplashScreenActivity
	{
        public MainActivity() : base(Resource.Layout.Main)
        { }

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
		}
	}
}



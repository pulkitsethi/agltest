﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using CatNames.Core.ViewModels;

namespace CatNames.Droid.Views.Activities
{
    [Activity(Label = "Pets list")]
    public class CatNamesActivity : BaseActivity<CatNamesViewModel>
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // TODO: changes
            SetContentView(Resource.Layout.CatNames);
        }
    }
}
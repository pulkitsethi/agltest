﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Core.ViewModels;

namespace CatNames.Droid.Views.Activities
{
    public class BaseActivity<T> : MvxCachingFragmentCompatActivity<T> where T: MvxViewModel
    {
        public override void SetContentView(int layoutResId)
        {
            var rootView = this.BindingInflate(layoutResId, null);

            base.SetContentView(rootView);
        }
    }
}
using MvvmCross.Platform.Plugins;

namespace CatNames.Droid.Bootstrap
{
    public class VisibilityPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Visibility.PluginLoader>
    {
    }
}
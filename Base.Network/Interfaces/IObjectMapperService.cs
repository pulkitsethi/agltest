﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Network.Interfaces
{
    public interface IObjectMapperService
    {
        T DeserializeObject<T>(string serializedObject);

        string SerializeObject(object toConvert);
    }
}

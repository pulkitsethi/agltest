﻿using Base.Network.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Network.Interfaces
{
    public interface INetworkService
    {
        Task<ApiResult<T>> GetAsync<T>(string resource);
    }
}

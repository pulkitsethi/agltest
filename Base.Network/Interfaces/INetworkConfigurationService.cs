﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Network.Interfaces
{
    public interface INetworkConfigurationService
    {
        int Timeout { get; set; }

        int NetworkRetryCount { get; set; }

        string ApiBase { get; set; }
    }
}

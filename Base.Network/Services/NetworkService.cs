﻿using Base.Network.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Network.Model;
using Polly;
using System.Net;
using System.Net.Http;
using ModernHttpClient;

namespace Base.Network.Services
{
    public class NetworkService : INetworkService
    {
        #region readonly

        private readonly Lazy<HttpClient> _httpClient;
        private readonly INetworkConfigurationService _configurationService;
        private readonly IObjectMapperService _objectMapperService;

        #endregion

        #region Constructor

        public NetworkService(INetworkConfigurationService configurationService,
            IObjectMapperService objectMapperService)
        {
            _configurationService = configurationService;
            _objectMapperService = objectMapperService;

            _httpClient = new Lazy<HttpClient>(InitHandler);
        }

        #endregion

        #region INetworkConfigurationService

        public async Task<ApiResult<T>> GetAsync<T>(string resource)
        {
            return await Policy
                .Handle<WebException>()
                .Or<TaskCanceledException>()
                .WaitAndRetryAsync(_configurationService.NetworkRetryCount, attemptNum => TimeSpan.FromSeconds(attemptNum))
                .ExecuteAsync<ApiResult<T>>(async () => await ExecuteGetAsync<T>(resource))
                .ConfigureAwait(false);
        }

        #endregion

        #region Implementation

        private HttpClient InitHandler()
        {
            var client = new HttpClient(new NativeMessageHandler())
            {
                Timeout = TimeSpan.FromMilliseconds(_configurationService.Timeout),
                BaseAddress = new Uri(_configurationService.ApiBase)
            };

            return client;
        }

        private async Task<ApiResult<T>> ExecuteGetAsync<T>(string resource)
        {
            ApiResult<T> result = new ApiResult<T>
            {
                Result = false,
                Message = string.Empty,
                Data = default(T),
            };

            try
            {
                var response = await _httpClient.Value.GetAsync(resource)
                    .ConfigureAwait(false);
                var content = await response.Content.ReadAsStringAsync()
                    .ConfigureAwait(false);

                result.Data = _objectMapperService.DeserializeObject<T>(content);
                result.Result = true;
            }
            catch (Exception exp)
            {
                result.Message = exp.Message;
            }

            return result;
        }

        #endregion
    }
}

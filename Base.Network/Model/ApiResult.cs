﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Network.Model
{
    public class ApiResult<T>
    {
        public bool Result { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }
    }
}
